/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_bonus.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svatostop <svatostop@student.42.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/21 14:50:27 by lgorilla          #+#    #+#             */
/*   Updated: 2020/06/28 15:39:22 by svatostop        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line_bonus.h"

int				del_join(char **line, char *buf, char **ost, int *flag)
{
	char	*ptr;
	char	*tmp;

	if ((ptr = ft_strchr(buf, '\n')))
	{
		*ptr = '\0';
		ptr++;
		if ((*ost = ft_strdup(ptr)) == NULL)
			return (-1);
		*flag = 1;
	}
	tmp = *line;
	if ((*line = ft_strjoin(*line, buf)) == NULL)
	{
		free(tmp);
		return (-1);
	}
	free(tmp);
	return (0);
}

int				checker_line_ost(char **line, char **ost)
{
	char	*ptr;
	char	*tmp;

	if (*ost != NULL)
	{
		if ((ptr = ft_strchr(*ost, '\n')))
		{
			*ptr = '\0';
			*line = ft_strdup(*ost);
			tmp = *ost;
			*ost = ft_strdup(++ptr);
			free(tmp);
			if (!(*line) || !(*ost))
				return (-1);
			return (1);
		}
		*line = ft_strdup(*ost);
		free(*ost);
		*ost = NULL;
		if (!(*line))
			return (-1);
	}
	else if (!(*line = ft_strdup("")))
		return (-1);
	return (0);
}

int				get_line(int fd, char **line, char **ost)
{
	int			res;
	int			checker;
	int			flag;
	char		buf[BUFFER_SIZE + 1];

	if ((checker = checker_line_ost(line, ost)) != 0)
		return (checker);
	flag = 0;
	while (!flag && (res = read(fd, buf, BUFFER_SIZE)) > 0)
	{
		buf[res] = '\0';
		if ((del_join(line, buf, ost, &flag)) == -1)
			return (-1);
	}
	if (res < 0)
		return (-1);
	return ((res || *ost) ? 1 : 0);
}

t_gnl_struct	*new_list(int fd)
{
	t_gnl_struct *new;

	if (!(new = (t_gnl_struct *)malloc(sizeof(t_gnl_struct))))
		return (NULL);
	new->fd = fd;
	new->ostatok = NULL;
	new->next = NULL;
	return (new);
}

int				get_next_line(int fd, char **line)
{
	static t_gnl_struct	*head;
	t_gnl_struct		*tmp;
	t_gnl_struct		*tmp1;
	int					result;

	if (!(tmp1 = NULL) && (fd < 0 || !line || BUFFER_SIZE < 1))
		return (-1);
	result = 0;
	if (head == NULL)
		if ((head = new_list(fd)) == NULL)
			return (-1);
	tmp = head;
	while (tmp && tmp->fd != fd)
	{
		if (tmp->next == NULL)
			if (((tmp->next = new_list(fd)) == NULL) &&
			remove_lst(&head, &tmp, tmp1, 'a'))
				return (-1);
		tmp = tmp->next;
	}
	if ((result = get_line(tmp->fd, line, &(tmp->ostatok))) == 0)
		remove_lst(&head, &tmp, tmp1, 'o');
	if ((result == -1) && remove_lst(&head, &tmp, tmp1, 'a') && *line)
		free(*line);
	return (result);
}
