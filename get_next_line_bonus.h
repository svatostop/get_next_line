/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_bonus.h                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: svatostop <svatostop@student.42.fr>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/06/21 14:53:24 by lgorilla          #+#    #+#             */
/*   Updated: 2020/06/28 15:36:44 by svatostop        ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_BONUS_H
# define GET_NEXT_LINE_BONUS_H

# include <unistd.h>
# include <stdlib.h>

typedef struct			s_get_n_line
{
	int					fd;
	char				*ostatok;
	struct s_get_n_line	*next;
}						t_gnl_struct;

char					*ft_strchr(const char *str, int ch);
size_t					ft_strlcpy(char *dst, const char *src, size_t size);
char					*ft_strdup(const char *str);
char					*ft_strjoin(char const *s1, char const *s2);
int						get_next_line(int fd, char **line);
int						get_line(int fd, char **line, char **ost);
t_gnl_struct			*new_list(int fd);
int						remove_lst(t_gnl_struct **head, t_gnl_struct **tmp,
							t_gnl_struct *tmp1, char mode);
int						checker_line_ost(char **line, char **ost);
int						del_join(char **line, char *buf, char **ost, int *flag);

#endif
